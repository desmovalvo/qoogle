class Sparqlgen {

    constructor(fd){

	// store the file
	this.fd = fd;
	
	// requirements
	this.fs = require("fs");
	this.request = require("request");
	this.n3lib = require("n3");
	this.rdflib = require("rdflib")
	
	// read the server uri
	this.sparqlgen = fd["sparql-generate"]["host"];
		
    }
    
    searchT(where, keywords){

	// sanitize keywords
	var ekeywords = escape(keywords);
	
	// get the content provider dictionary (cpd)
	var cpd = this.fd["content-providers"][where];

	// prepare the sparql-generate query (sgq)
	var qfd = this.fs.readFileSync(cpd["mappings"]["songs_by_name"]["query"], "utf8");
	var sgq = qfd.replace("$pattern", ekeywords).replace(/\$token/g, cpd["key"]);
	
	// prepare a request for sparql-generate
	console.log("Asking SPARQL-generate to look on " + where + "...");
	var options = {  
	    url: this.sparqlgen,
	    method: 'POST',
	    form: {"query": sgq}
	};
	
	// invoke sparql-generate with a promise
	var p = new Promise(function(resolve, reject){
	    var request = require("request");	   
	    request(options, function(err, res, body){
		if (err){
		    console.log("Error performing request to SPARQL-Generate");
		    reject("Error with request");
		} else {
		    console.log("Request to SPARQL-Generate succesful");
		    resolve(body);
		}
	    });
	}).then(
	    (data) => { return data }
	).catch(
	    (data) => { return null }
	);
	return p	
	
    }

    searchC(where, keywords){
	
	// sanitize keywords
	keywords = escape(keywords);
	
	// get the content provider dictionary (cpd)
	var cpd = this.fd["content-providers"][where];

	// prepare the sparql-generate query (sgq)
	var qfd = this.fs.readFileSync(cpd["mappings"]["collections_by_name"]["query"], "utf8");
	var sgq = qfd.replace(/\$token/g, cpd["key"]).replace("$pattern", keywords);
	
	// prepare a request for sparql-generate
	console.log("Asking SPARQL-generate to look on " + where + "...");
	console.log(sgq)
	var options = {  
	    url: this.sparqlgen,
	    method: 'POST',
	    form: {"query": sgq}
	};

	// invoke sparql-generate with a promise
	var p = new Promise(function(resolve, reject){
	    var request = require("request");	   
	    request(options, function(err, res, body){
		if (err){
		    console.log("Error performing request to SPARQL-Generate");
		    reject("Error with request");
		} else {
		    console.log("Request to SPARQL-Generate succesful");
		    resolve(body);
		}
	    });
	}).then(
	    (data) => { return data }
	).catch(
	    (data) => { return null }
	);
	return p	
	
    }    
    
}

module.exports.Sparqlgen = Sparqlgen;
