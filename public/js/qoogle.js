function search(what){
        
    // debug
    console.log("Invoked search function");
    console.log(document.getElementById("keyword").value);
    
    document.getElementById("resultsStats").innerHTML = "";
    rdiv = document.getElementById("results");
    rdiv.innerHTML = "<div align='center'><img src='images/loading.gif'></div>";
    
    // invoke the search action
    if (what === "tracks")
	searchURI = document.URL + "searchTracks";
    else
	searchURI = document.URL + "searchCollections";

    var req = {
	method: 'POST',
	url: searchURI,
	headers: {'Content-Type': 'application/json'},
	data: JSON.stringify({"keywords": document.getElementById("keyword").value, "synonyms":document.getElementById("vocab").checked})
    };

    $.ajax(req).
	then(function(response) {

            // when the response is available
            console.log(response);

	    // write results number
	    document.getElementById("resultsStats").innerHTML = Object.keys(response).length + " results"
	    
	    // get the div element devoted to host results
	    rdiv = document.getElementById("results");
	    rdiv.innerHTML = "";
	    for (r in response){
		rdiv.innerHTML += '<div class="card" border-primary mb-3>' +
                    '<div class="card-body">' +
		    '<h5 class="card-title">' + response[r]["name"] + '</h5><br>' +
		    '<b>Provided by:</b>&nbsp;<a href="' + response[r]["provenanceURI"] + '">' + response[r]["provenance"] + '</a><br>' +
		    '<i class="fab fa-itunes-note"></i>&nbsp;<a href="' + r + '">' + r + '</a></div></div><br>';
	    }
	    
	    // iterate on data and put song in the element
	    
	}, function(response) {
            // error.
	    
            //ok
	}, function(response) {
            // error.
	});
    
    
}

function getSource(uri){

    if (uri.search("jamendo") > "-1")
	return "Jamendo"
    if (uri.search("freesound") > "-1")
	return "Freesound"
    if (uri.search("europeana") > "-1")
	return "Europeana"    
}
