// base requirements
var path = require("path")
var fs = require("fs");

// rdflib
rdflib = require("rdflib")

// express reqs and conf
const express = require('express')
const app = express()
app.set('view engine', 'pug');

// body parser reqs and conf
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// sparql generate reqs and conf
var sparqlgen = require('./lib/sparqlgen');
fd = fs.readFileSync("mapping.json");
s = new sparqlgen.Sparqlgen(JSON.parse(fd));

// configuring routes for static files
app.use(express.static(__dirname + '/public'));

// configuring the main routes
app.get('/', function(req,res){
    
    // debug print
    console.log("Invoked / route");

    // return
    res.render('index', { title: "Qoogle",
			  img: "test.png"
			});
});

app.post("/searchTracks", function(req, res){

    // debug print
    console.log("Invoked /searchTracks route!");

    // get the keywords
    data = req.body;
    keywords = data["keywords"];
    synonyms = data["synonyms"];

    // init promise
    var p;
    
    // get synonyms
    if (synonyms){

	// invoke datamuse to get synonyms
	var ork = keywords.replace(/\ /g, "+");
	var url = "https://api.datamuse.com/words?ml=" + ork + "&max=5"

	// prepare a request for datamuse
	console.log("Asking datamuse to look for synonyms");
	var options = { url: url, method: 'GET'	};
	
	// invoke sparql-generate with a promise
	p = new Promise(function(resolve, reject){
	    var request = require("request");	   
	    request(options, function(err, res, body){
		if (err){
		    console.log("Error performing request to datamuse");
		    reject("Error with request");
		} else {
		    synList = [];
		    console.log("Request to datamuse succesful");
		    jbody = JSON.parse(body);
		    for (k in jbody){
			synList.push(jbody[k]["word"]);
		    }
		    resolve(synList);
		}
	    });
	})

    }
    else {
	p = new Promise(function(resolve, reject){
	    resolve([]);
	})
    }

    // invoke sparql-generate
    p.then(
	(data) => {

	    // initialize an empty local store
	    store = rdflib.graph();

	    // add synonyms keywords
	    // keywords = keywords + " " + data.join(" ")
	    data.unshift(keywords)
	    var ekeywords = data.join("+OR+");
	    var jkeywords = data.join("+")
	    var fkeywords = "\"" + data.join(" ") + "\""
	    console.log("KEYS:")
	    console.log(ekeywords);
	    console.log(fkeywords);
	    console.log(jkeywords);
	    
	    // invoke the research
	    console.log("Invoking promises");
	    jprom = s.searchT("jamendo", jkeywords);
	    eprom = s.searchT("europeana", ekeywords);
	    fprom = s.searchT("freesound", fkeywords);

	    // wait for completion of research
	    Promise.all([jprom, eprom, fprom]).then(
		values => {
		    
		    // debug print
		    console.log("Promises completed!");

		    // initialize results
		    output = {};
	    
		    // copy RDF results in the graph
		    for (value in values){
	    		rdflib.parse(values[value], store, "http://example.org/something", 'text/n3');	
		    }

		    // perform the SPARQL query and return results
		    var sparqlQuery = `PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                    PREFIX dc: <http://purl.org/dc/elements/1.1/>
                    PREFIX ac: <http://audiocommons.org/ns/audiocommons#>
                    PREFIX prov: <http://www.w3.org/ns/prov#>
                    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
	            SELECT ?clip ?title ?prov ?provName WHERE { ?clip rdf:type ac:AudioClip . ?clip dc:title ?title . ?clip prov:wasAttributedTo ?prov . ?prov foaf:name ?provName . }`;
		    var q = rdflib.SPARQLToQuery(sparqlQuery, false, store);
		    p = new Promise(
			function(resolve, reject){
			    var o = {};
			    var sqq = store.query(q,
						  // on results
						  function(result){
	    					      o[result["?clip"]["value"]] = { "name":result["?title"]["value"], "provenanceURI":result["?prov"]["value"], "provenance":result["?provName"]["value"]}},
						  
						  // fetcher
						  function(){},
						  
						  // on done
						  function(){
						      resolve(o);
						  }		   					      
						 )
			}
		    ).then(
			(data) => {
			    console.log("Ready to return data!");
			    res.send(data)}
		    ).catch(
			(data) => {
			    console.log("Error!");
			    res.send({})}
		    );		    	    
		}
	    );
	    
	}
    ).catch(
	(data) => { return null }
    );    
    
});


app.post("/searchCollections", function(req, res){

    // debug print
    console.log("Invoked /searchCollections route!");

    // get the keywords
    data = req.body;
    keywords = data["keywords"];
    
    // initialize an empty local store
    var store = rdflib.graph();

    // invoke the research
    jprom = s.searchC("jamendo", keywords);
    fprom = s.searchC("freesound", keywords);

    // wait for completion of research
    Promise.all([jprom, fprom]).then(
	values => {

	    // debug print
	    console.log("Promises completed!");

	    // initialize results
	    output = {};
	    
	    // copy RDF results in the graph
	    console.log(values.length)
	    for (value in values){
		console.log(values[value])
	    	rdflib.parse(values[value], store, "http://example.org/something", 'text/n3');	
	    }

	    // perform the SPARQL query and return results
	    var sparqlQuery = `PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX ac: <http://audiocommons.org/ns/audiocommons#>
	    SELECT ?col ?title WHERE { ?col rdf:type ac:AudioCollection . ?col dc:title ?title . }`;
	    var q = rdflib.SPARQLToQuery(sparqlQuery, false, store);
	    p = new Promise(
		function(resolve, reject){
		    var o = {};
		    var sqq = store.query(q,
					  // on results
					  function(result){
	    				      o[result["?col"]["value"]] = result["?title"]["value"];},

					  // fetcher
					  function(){},

					  // on done
					  function(){resolve(o)})
		}
	    ).then(
		(data) => {
		    console.log("Ready to return data!");
		    res.send(data)}
	    ).catch(
		(data) => {
		    console.log("Error!");
		    res.send({})}
	    );		    	    
	}
    );
    
});

// listening...
app.listen(8000, () => console.log("Qoogle ready on port 8000!")
)
